let settings={
    ip:[
        'http://localhost:8000/',
        'http://192.168.0.118:8000/',
        'http://85.196.141.225/',
        'https://pgmetlib.000webhostapp.com/'
    ],
    env:'test',
}
let menu=[
    {
        to:'/books/',
        name:'Книги',
        icon:'mdi-book'
    },
    {
        to:'/library/',
        name:'Твоята Библиотека',
        icon:'mdi-library'
    },
    {
        to:'/profile/',
        name:'Профил',
        icon:'mdi-account-circle'
    },
    {
        to:'/about/',
        name:'За нас',
        icon:'mdi-information'
    },

]


function getEnv(){
    if(settings.env == 'dev'){
        return settings.ip[0];
    }else{
        return settings.ip[3];
    }
}
export  {settings, getEnv,menu};
