import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)
const store = new Vuex.Store({
    state: {
        user:null,
    },
    mutations: {
        setUser (state,user) {
            state.user = user;
        },
        unsetUser (state) {
            state.user = null;
        },


    },
    getters:{
        getUser:state => {
            return state.user;
        },

    },
    actions:{

    }
});
export default store;
