require('./bootstrap');
import Vue from 'vue'
import App from "./componets/App";
import router from './router/router'
import vuetify from './vuetify/vuetify' // path to vuetify export
import VueRouter from 'vue-router'
import mix from  './mixins/mixin'
import axios from 'axios'
import store from './store/store'
import {getEnv} from './env.js';

Vue.use(VueRouter);
Vue.mixin(mix);
Vue.prototype.$EventBus = new Vue();
axios.defaults.baseURL = getEnv();


window.Vue = require('vue');



const app = new Vue({
    el: '#app',
    vuetify,
    store,
    router,
    template: '<App/>',
    components: { App }

});

