import Vue from 'vue'
import Router from 'vue-router'
import Login from "../componets/Login";
import Books from "../componets/Books/Books";
import Book from "../componets/Books/Book";
import store from '../store/store'
import Library from "../componets/Library/Library";
import Profile from "../componets/Profile/Profile";
import PasswordReset from "../componets/Profile/PasswordReset";
import Tutorial from "../componets/Tutorial/Tutorial";
let router =  new Router({
     mode:'history',
    routes: [
        {
             path:'/',
            name:'Login',
            component:Login
        },
        {
             path:'/home',
            name:'Login',
            component:Login
        },
        {
             path:'/books',
            name:'Books',
            component:Books,
            meta: { auth: true }

        },
        {
            path:'/books/book/:id',
            name:'Book',
            component:Book,
            props:true,
            meta: { auth: true }

        },
        {
            path:'/library',
            name:'Library',
            component:Library,
           meta: { auth: true }

        },
        {
            path:'/profile',
            name:'Profile',
            component:Profile,
           meta: { auth: true }

        },
        {
            path:'/tutorial',
            name:'Tutorial',
            component:Tutorial,
           meta: { auth: true }

        },
        {
            path:'/profile/password/reset/code/:code',
            name:'PasswordReset',
            component:PasswordReset,
            props:true,
           //meta: { auth: true }

        },

    ]
});
router.beforeEach((to, from, next) => {
    if (to.matched.some(record => record.meta.auth)) {
        // this route requires auth, check if logged in
        // if not, redirect to login page.
        if (store.state.user == null) {
            next({
                path: '/',
            })
        } else {
            next()
        }
    } else {
        next() // make sure to always call next()!
    }
})
export default router;

