<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Въстановяване на паролата</title>
</head>
<body>
<p>Линк за въстановяване на паролата</p>
<a href="{{ URL::to('/profile/password/reset/code/' . $code) }}">Тук</a>
</body>
</html>
