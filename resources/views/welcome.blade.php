<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Library</title>

        <!-- Fonts -->



        <!-- Styles -->
        <link rel="manifest" href="manifest.json">

        <meta name="mobile-web-app-capable" content="yes">
        <meta name="apple-mobile-web-app-capable" content="yes">
        <meta name="application-name" content="PGMETLib">
        <meta name="apple-mobile-web-app-title" content="PGMETLib">
        <meta name="theme-color" content="#1cad1e">
        <meta name="msapplication-navbutton-color" content="#1cad1e">
        <meta name="apple-mobile-web-app-status-bar-style" content="black-translucent">
        <meta name="msapplication-starturl" content="/">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

        <link rel="icon" type="png" href="imgs/logo.png">
        <link rel="apple-touch-icon" type="png" href="imgs/logo.png">

    </head>
    <body>
      <div id="app">

      </div>
    </body>
    <script src="{{asset('/js/app.js')}}"></script>
    <script src="{{asset('service-worker.js')}}"></script>
    <script >
        if ('serviceWorker' in navigator) {
            navigator.serviceWorker.register('/service-worker.js', {scope: ''})
                .then((reg) => {
                    // registration worked
                    console.log('Registration succeeded. Scope is ' + reg.scope);
                }).catch((error) => {
                // registration failed
                console.log('Registration failed with ' + error);
            });
        }
    </script>
    <link href="https://fonts.googleapis.com/css?family=Roboto:100,300,400,500,700,900" rel="stylesheet">
    <link href="https://cdn.jsdelivr.net/npm/@mdi/font@4.x/css/materialdesignicons.min.css" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/material-design-iconic-font/2.2.0/css/material-design-iconic-font.min.css">
</html>
