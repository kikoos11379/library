<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserBooks extends Model
{
    //
    protected $table = 'user_books';
    protected $fillable = ['user_id','book_id'];

    public  function AddBook($user_id,$book_id){
       return  $this->create([
            'user_id'=>$user_id,
            'book_id'=>$book_id
        ]);
    }
}
