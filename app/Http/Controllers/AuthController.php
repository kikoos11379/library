<?php

namespace App\Http\Controllers;


use App\Http\Requests\PasswordResetRequest;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;

class AuthController extends Controller
{
    //
    public function __construct()
    {
        $this->middleware('auth:api', ['except' => ['login']]);
    }

    /**
     * Get a JWT via given credentials.
     *
     * @return \Illuminate\Http\JsonResponse
     */
   private function sendMail($code,$mail){
        Mail::send('mail', ['code'=>$code], function ($m) use ($mail) {
            $m->from('waikovixstudio@gmail.com', 'PGMETLib');
            $m->to($mail)
                ->subject('Промяна на паролата');
        });
    }
    public function login()
    {
        $credentials = request(['username', 'password']);

        if (! $token = auth()->attempt($credentials)) {
            return response()->json(['error' => 'Unauthorized'], 401);
        }

        return $this->respondWithToken($token);
    }

    /**
     * Get the authenticated User.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function me()
    {
        return response()->json(auth()->user());
    }

    /**
     * Log the user out (Invalidate the token).
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function logout()
    {
        auth()->logout();

        return response()->json(['message' => 'Successfully logged out']);
    }

    /**
     * Refresh a token.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function refresh()
    {
        return $this->respondWithToken(auth()->refresh());
    }
    public function resetPassword()
    {
        $code = str_random(30);
        $user = auth('api')->user();
        $user->code = $code;
        $user->save();
        if($user->email == null){
            return response()->json([
               'type'=>'err',
                'msg'=>'Въведи първо email!'
            ],400);
        }
        $this->sendMail($code,$user->email);
        return response()->json([
            'type'=>'succ',
            'msg'=>'Ok'
        ],200);
    }

    public function confirmCode(PasswordResetRequest $request){
        $user = User::where('code',$request->code)->first();

        if(empty($user)){
            return response()->json([
                'type'=>'err',
                'msg'=>'Грешен код'
            ],400);
        }
        if(bcrypt($request->password) == $user->password){
            return response()->json([
                'type'=>'err',
                'msg'=>'Паролата не може да бъде същата като сегашната парола'
            ],400);
        }

        $user->password = bcrypt($request->password);
        $user->code = null;
        $user->save();
        $this->logout();

        return response()->json([
            'type'=>'succ',
            'msg'=>'Ok'
        ],200);
    }

    /**
     * Get the token array structure.
     *
     * @param  string $token
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function respondWithToken($token)
    {
        return response()->json([
            'access_token' => $token,
            'token_type' => 'bearer',
            'expires_in' => auth('api')->factory()->getTTL() * 60
        ]);
    }
}
