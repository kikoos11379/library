<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\SubcategoryRequest;
use App\Models\Category;
use App\Models\Subcategory;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;
use Illuminate\Http\Request;

/**
 * Class SubcategoryCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class SubcategoryCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;

    public function setup()
    {
        $this->crud->setModel('App\Models\Subcategory');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/subcategory');
        $this->crud->setEntityNameStrings('subcategory', 'subcategories');
    }

    protected function setupListOperation()
    {
        // TODO: remove setFromDb() and manually define Columns, maybe Filters
       // $this->crud->setFromDb();
        $this->crud->addColumns([
            [
              'name'=>'name',
              'type'=>'text',
                'label'=>'Име'
            ],
            [
                'label' => "Категория", // Table column heading
                'type' => "select",
                'name' => 'category_id', // the column that contains the ID of that connected entity;
                'entity' => 'category', // the method that defines the relationship in your Model
                'attribute' => "name", // foreign key attribute that is shown to user
                'model' => "App\Models\Category", // foreign key model
            ]
        ]);
    }

    protected function setupCreateOperation()
    {
        $this->crud->setValidation(SubcategoryRequest::class);

        // TODO: remove setFromDb() and manually define Fields
//        $this->crud->setFromDb();
        $this->crud->addFields([
            [  // Select
                'label' => "Име",
                'type' => 'text',
                'name' => 'name', // the db column for the foreign key

            ],
            [
                // Select
                'label' => "Категория",
                'type' => 'select',
                'name' => 'category_id', // the db column for the foreign key
                'entity' => 'category', // the method that defines the relationship in your Model
                'attribute' => 'name', // foreign key attribute that is shown to user
                'model' => "App\Models\Category",
            ],

        ]);
    }

    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }
    public function getCategories(Request $request)
    {
        $search_term = $request->input('q');
        $form = collect($request->input('form'))->pluck('value', 'name');

        $options = Subcategory::query();

        // if no category has been selected, show no options
        if (! $form['category_id']) {
            return [];
        }

        // if a category has been selected, only show articles in that category
        if ($form['category_id']) {
            $options = $options->where('id', $form['category_id']);
        }

        if ($search_term) {
            $results = $options->where('name', 'LIKE', '%'.$search_term.'%')->paginate(10);
        } else {
            $results = $options->paginate(10);
        }

        return $options->paginate(10);
    }
    public function getCategory($id)
    {
       return Subcategory::find($id);
    }
}
