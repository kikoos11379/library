<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\UserRequest;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;
use Backpack\PermissionManager\app\Http\Controllers\UserCrudController as OriginalUserCrudController;
use Illuminate\Http\Request;
/**
 * Class UserCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class UserCrudController extends OriginalUserCrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;

    public function setup()
    {
        $this->crud->setModel('App\User');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/user');
        $this->crud->setEntityNameStrings('user', 'users');
    }

    public function setupListOperation()
    {
        // TODO: remove setFromDb() and manually define Columns, maybe Filters
        //$this->crud->setFromDb();
        $this->crud->addColumns([
            [
                'name'=>'name',
                'type'=>'text',
                'label'=>' Име'
            ],
            [
                'name'=>'username',
                'type'=>'text',
                'label'=>'Потребителско име'
            ],

            [
                'name'=>'email',
                'type'=>'text',
                'label'=>'Email'
            ],
        ]);
    }

    public function setupCreateOperation()
    {
        $this->crud->setValidation(UserRequest::class);

        // TODO: remove setFromDb() and manually define Fields
        // $this->crud->setFromDb();
        $this->crud->addFields([
            [
                'name' => 'name',
                'type' => 'text',
                'label' => ' Име'
            ],
            [
                'name' => 'username',
                'type' => 'text',
                'label' => 'Потребителско име'
            ],

            [
                'name' => 'email',
                'type' => 'email',
                'label' => 'Email'
            ],
            [
                'name' => 'password',
                'type' => 'password',
                'label' => 'Парола'
            ],
            [
                // two interconnected entities
                'label'            => 'Роля',
                'name'             => 'roles', // the method that defines the relationship in your Model
                'entity'           => 'roles', // the method that defines the relationship in your Model
                'entity_secondary' => 'permissions', // the method that defines the relationship in your Model
                'attribute'        => 'name', // foreign key attribute that is shown to user
                'model'            => config('permission.models.role'), // foreign key model
                'pivot'            => true, // on create&update, do you need to add/delete pivot table entries?]
                'number_columns'   => 3, //can be 1,2,3,4,6
                'type'              => 'checklist',

            ]

        ]);

    }

    public function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }
    public function getBooks(){
        $books = auth('api')->user()->books;
        return response()->json([
            'books'=>$books
        ]);
    }
    public function addBook(Request $request){
        $book = auth('api')->user()->books()->attach($request->book_id);
        return response()->json([
            'status'=>'ok'
        ],200);
    }
    public function removeBook($book_id){
        $book = auth('api')->user()->books()->detach($book_id);
        return response()->json([
            'status'=>'ok'
        ],200);
    }
}
