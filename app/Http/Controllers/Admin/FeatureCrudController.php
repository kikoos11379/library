<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\FeatureRequest;
use App\Models\Feature;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;

/**
 * Class FeatureCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class FeatureCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;

    public function setup()
    {
        $this->crud->setModel('App\Models\Feature');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/feature');
        $this->crud->setEntityNameStrings('feature', 'features');
    }

    protected function setupListOperation()
    {
        // TODO: remove setFromDb() and manually define Columns, maybe Filters
        $this->crud->setFromDb();
    }

    protected function setupCreateOperation()
    {
        $this->crud->setValidation(FeatureRequest::class);

        // TODO: remove setFromDb() and manually define Fields
       // $this->crud->setFromDb();
        $this->crud->addFields([
            [
                'name'=>'title',
                'label'=>'Заглавие',
                'type'=>'text'
            ],
            [
                'name'=>'description',
                'label'=>'Описание',
                'type'=>'text'
            ],
            [
                'name'=>'icon',
                'label'=>'Икона',
                'type'=>'icon_picker',
                'iconset' => 'materialdesign'
            ],
        ]);
    }

    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }
    public function getFeatures(){
        $features = Feature::latest()->get();
        return response()->json([
           'features'=>$features
        ]);
    }
}
