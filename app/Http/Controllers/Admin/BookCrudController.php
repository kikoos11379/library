<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\BookRequest;
use App\Models\Book;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;

/**
 * Class BookCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class BookCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;

    public function setup()
    {
        $this->crud->setModel('App\Models\Book');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/book');
        $this->crud->setEntityNameStrings('book', 'books');
    }

    protected function setupListOperation()
    {
        // TODO: remove setFromDb() and manually define Columns, maybe Filters
       // $this->crud->setFromDb();
        $this->crud->addColumns([
            [
              'name'=>'image',
              'type'=>'image',
              'label'=>'Снимка'
            ],
           [
               'name'=>'name',
               'type'=>'text',
               'label'=>'Заглавие'
           ],
            [
                'name'=>'description',
                'type'=>'text',
                'label'=>'Описание'
            ],
            [
                'name'=>'author',
                'type'=>'text',
                'label'=>'Автор'
            ],
            [
            'label' => "Категория", // Table column heading
            'type' => "select",
            'name' => 'category_id', // the column that contains the ID of that connected entity;
            'entity' => 'category', // the method that defines the relationship in your Model
            'attribute' => "name", // foreign key attribute that is shown to user
            'model' => "App\Models\Category", // foreign key model
            ]
        ]);
    }

    protected function setupCreateOperation()
    {
        $this->crud->setValidation(BookRequest::class);

        // TODO: remove setFromDb() and manually define Fields
        //$this->crud->setFromDb();
        $this->crud->addFields([
        [
          'name'=>'name',
          'label'=>'Заглавие',
          'type'=>'text'
        ],
        [
          'name'=>'description',
          'label'=>'Описание',
          'type'=>'tinymce'
        ],
        [
          'name'=>'author',
          'label'=>'Автор',
          'type'=>'text'
        ],
        [
          'name'=>'url',
          'label'=>'Качи файл(pdf)',
          'type'=>'upload'
        ],
        [
          'name'=>'url',
          'label'=>'Качи файл(pdf)',
          'type'=>'upload',
        ],
        [
          'name'=>'image',
          'label'=>'Снимка',
          'type'=>'image',
          'upload' => true,

        ],
            [  // Select
                'label' => "Category",
                'type' => 'select',
                'name' => 'category_id', // the db column for the foreign key
                'entity' => 'category', // the method that defines the relationship in your Model
                'attribute' => 'name', // foreign key attribute that is shown to user
                'model' => "App\Models\Category",
            ],
            [ // select2_from_ajax: 1-n relationship
                'label'                => "Подкатегория", // Table column heading
                'type'                 => 'select2_from_ajax',
                'name'                 => 'subcategory_id', // the column that contains the ID of that connected entity;
                'entity'               => 'subcategory', // the method that defines the relationship in your Model
                'attribute'            => 'name', // foreign key attribute that is shown to user
                'data_source'          => url('api/subcategory'), // url to controller search function (with /{id} should return model)
                'placeholder'          => 'Избери си подкатегория', // placeholder for the select
                'minimum_input_length' => 0, // minimum characters to type before querying results
                'dependencies'            => ['category']
                ]
        ]);
    }

    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }
    public function getBooks($category,$subcategory,$search){
        $books=Book::query();
        if ($category!='*') {
            $books =$books->where('category_id','=',$category);
        }
        if ($subcategory!='*') {
            $books =$books->where('subcategory_id','=',$subcategory);
        }
        if ($search!='*') {
            $books=$books->where('name','like','%'.$search.'%');
        }

        $books =$books->paginate();




        return response()->json([
           'books'=>$books
        ]);

    }
    public function getBook($book_id)
    {
        $book = Book::find($book_id);
        return response()->json([
           'book'=>$book,
            'has'=>$book->assignedToUser()
        ]);
    }
}
