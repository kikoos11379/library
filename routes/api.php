<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::group([

    'middleware' => 'api',
    'prefix' => 'auth'

], function ($router) {

    Route::post('login', 'AuthController@login');
    Route::post('logout', 'AuthController@logout');
    Route::post('refresh', 'AuthController@refresh');
    Route::get('user', 'AuthController@me');
    Route::get('user/books', 'Admin\UserCrudController@getBooks');
    Route::post('user/books/add', 'Admin\UserCrudController@addBook');
    Route::delete('user/books/book/{book_id}', 'Admin\UserCrudController@removebook');
    Route::post('user/password/reset/', 'AuthController@resetPassword');
    Route::post('user/password/reset/code/', 'AuthController@confirmCode');

    Route::get('/books/category/{category}/subcategory/{subcategory}/search/{search}/','Admin\BookCrudController@getBooks');
    Route::get('/book/{book_id}','Admin\BookCrudController@getBook');



});
Route::get('/subcategory', 'Admin\SubcategoryCrudController@getCategories');
Route::get('/categories', 'Admin\CategoryCrudController@getCategories');
Route::get('/subcategory/{id}','Admin\SubcategoryCrudController@getCategory');



Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
