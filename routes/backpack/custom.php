<?php

// --------------------------
// Custom Backpack Routes
// --------------------------
// This route file is loaded automatically by Backpack\Base.
// Routes you generate using Backpack\Generators will be placed here.

Route::group([
    'prefix'     => config('backpack.base.route_prefix', 'admin'),
    'middleware' => ['web', config('backpack.base.middleware_key', 'admin')],
    'namespace'  => 'App\Http\Controllers\Admin',
], function () { // custom admin routes
    Route::crud('category', 'CategoryCrudController');
    Route::crud('category', 'CategoryCrudController');
    Route::crud('book', 'BookCrudController');
    Route::crud('book', 'BookCrudController');
    Route::crud('feature', 'FeatureCrudController');
    Route::crud('feature', 'FeatureCrudController');

    Route::crud('subcategory', 'SubcategoryCrudController');
    Route::crud('subcategory', 'SubcategoryCrudController');
    Route::crud('user', 'UserCrudController');
    Route::crud('user', 'UserCrudController');
}); // this should be the absolute last line of this file